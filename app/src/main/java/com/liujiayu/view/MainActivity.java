package com.liujiayu.view;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.liujiayu.view.cirque.CirqueView;
import com.liujiayu.view.cirque.CirqueView2;
import com.liujiayu.view.utils.Util;

import java.util.Random;


public class MainActivity extends Activity {
    CirqueView2 mCv2 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.activity_main);

        CirqueView mCv = findViewById(R.id.cv);
        mCv2 = findViewById(R.id.cv2);
//        mCv.setTemperaturemin(-30, 30); //设置温度范围  默认10-30
//        mCv.setTime(0, 60); //设置时间范围  默认10-30
        mCv.setDefault(27, 22);  //添加默认数据--注:不能超出范围
        mCv.setTxtFinishListener(new CirqueView.txtFinishListener() {
            @Override
            public void onFinish(int temperature, int time) {
                Util.showToast(MainActivity.this, temperature + "//" + time);
            }
        });
        mCv2.setDefault(220, 24);  //添加默认数据--注:不能超出范围
        mCv2.setTurnFinishListener(new CirqueView2.OnTurnFinishListener() {
            @Override
            public void onTemperatureChanged(boolean isTemp, int tempeOrPower) {
                Util.showToast(MainActivity.this, "温度："+tempeOrPower );
                Log.d("OnTurnFinishListener", "onTimeChanged: "+tempeOrPower);
            }

            @Override
            public void onTimeChanged(int time) {
                Log.d("OnTurnFinishListener", "onTimeChanged: "+time);
                Util.showToast(MainActivity.this,  "时间："+time );
            }
        });
    }


    public void clickBtn(View view) {
        mCv2.setDefault(randInt(10,30),randInt(10,30));
    }
    public int randInt(int min, int max) {

        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
}
