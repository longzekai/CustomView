package com.liujiayu.view.cirque;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.liujiayu.view.R;
import com.ruffian.library.widget.RLinearLayout;

/**
 * ================================================
 * 作    者：Danny
 * 版    本：1.0
 * 创建日期：2019/12/27 0027-14:50
 * 描    述：设备连接状态控制按钮
 * 修订历史：
 * ================================================
 */
public class ConnectButtonState extends RLinearLayout {

    public static final int DEVICE_CONNECTING = 1;
    public static final int DEVICE_CONNECTED = 2;
    public static final int DEVICE_DISCONNECTED = 3;
    public static final int BLE_OFF_STATE = 4;
    private ProgressBar mConnecting;
    private ImageView mConnected;
    private TextView mText;

    private Context context;

    public ConnectButtonState(Context context) {
        this(context, null);
    }

    public ConnectButtonState(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public ConnectButtonState(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, AttributeSet attrs, int defStyle) {
        this.context = context ;
        LayoutInflater.from(context).inflate(R.layout.cook_btn_connect_state, this, true);
        this.setOrientation(LinearLayout.HORIZONTAL);
        this.setGravity(Gravity.CENTER_HORIZONTAL);
        this.getHelper().setCornerRadius(dpToPx(6));
        this.getHelper().setBackgroundColorNormal(context.getResources().getColor(R.color.cook_FF8008));
        this.getHelper().setBackgroundColorPressed(context.getResources().getColor(R.color.cook_999999));
        this.getHelper().setBackgroundColorUnable(context.getResources().getColor(R.color.cook_F0F0F0));
        mConnecting = findViewById(R.id.cook_pb_connecting);
        mConnected = findViewById(R.id.cook_iv_connected);
        mText = findViewById(R.id.cook_tv_connect_state);
        TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.cook_connected_state, defStyle, 0);
        if (attributes != null) {
            int mConnectState = attributes.getInteger(R.styleable.cook_connected_state_cook_button_state, DEVICE_DISCONNECTED);
            setConnectViewState(mConnectState);
            attributes.recycle();
        }
    }
    private float dpToPx(int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getContext().getResources().getDisplayMetrics());
    }
    public void setConnectViewState(int state) {
        if (state < 0 || state > 4) state = DEVICE_DISCONNECTED ;
        if (state == DEVICE_CONNECTED) {
            this.setEnabled(false);
            mConnecting.setVisibility(View.GONE);
            mConnected.setVisibility(View.VISIBLE);
            mText.setTextColor(context.getResources().getColor(R.color.cook_999999));
            mText.setText("已连接");
        } else if (state == DEVICE_DISCONNECTED) {
            this.setEnabled(true);
            mConnecting.setVisibility(View.GONE);
            mConnected.setVisibility(View.GONE);
            mText.setTextColor(context.getResources().getColor(R.color.colorWhite));
            mText.setText("连接设备");
        } else if (state == DEVICE_CONNECTING) {
            this.setEnabled(false);
            mConnecting.setVisibility(View.VISIBLE);
            mConnected.setVisibility(View.GONE);
            mText.setTextColor(context.getResources().getColor(R.color.cook_999999));
            mText.setText("连接中");
        } else if (state == BLE_OFF_STATE) {
            this.setEnabled(false);
            mConnecting.setVisibility(View.GONE);
            mConnected.setVisibility(View.GONE);
            mText.setTextColor(context.getResources().getColor(R.color.cook_999999));
            mText.setText("连接设备");
        }
    }
}
