package com.liujiayu.view.cirque;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.liujiayu.view.R;


/**
 * ================================================
 * 作    者：Danny
 * 版    本：1.0
 * 创建日期：2019/10/12 0012-17:15
 * 描    述：柱状图
 * 修订历史：
 * ================================================
 */
public class CustomRetangeView extends View {

    /**
     * 重量
     */
    protected int weight;
    /**
     * 默认的最大值
     */
    protected int defaultNum;

    /**
     * 百分比
     */
    protected int recProgress;

    /**
     * 外面柱子默认颜色
     */
    protected int recColor;
    /**
     * 里面柱子颜色
     */
    protected int insideColor;

    /**
     * 数量文字颜色
     */
    protected int numberTxtColor;
    /**
     * 百分比文字的颜色
     */
    protected int percentTxtColor;

    /**
     * 数量文字的大小:单位sp
     */
    protected int numberTextSize;
    /**
     * 百分比文字的大小:单位sp
     */
    protected int percentTextSize;

    protected final Paint paint;

    /**
     * 控件的宽、高
     */
    private int width, height;

    /**
     * 外面柱子高度
     */
    private int recHeight;
    /**
     * 里面柱子高度
     */
    private int insideHeight;
    /**
     * 柱子宽度
     */
    private int rowWidth = 30;

    private Rect rec;


    private int[] liquidColors = {
            Color.parseColor("#F2F2F2"),
            Color.parseColor("#E6E6E6"),
            Color.parseColor("#CCCCCC"),
            Color.parseColor("#B8B8B8"),
            Color.parseColor("#AAAAAA"),
    };

    private float[] liquidPosition = {
           0.0f,
            0.21f,
            0.42f,
            0.63f,
            0.84f
    };

    private int[] solidGradient = {Color.parseColor("#F2F2F2"), Color.parseColor("#E6E6E6"), Color.parseColor("#CCCCCC"), Color.parseColor("#B8B8B8"),};

    public CustomRetangeView(Context context) {
        this(context, null);
    }

    public CustomRetangeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.CustomRetangeView);
        int defaultColor = ContextCompat.getColor(getContext(), R.color.green);
        int defaultInsideColor = ContextCompat.getColor(getContext(), R.color.red);
        recColor = array.getColor(R.styleable.CustomRetangeView_defaultColor, defaultColor);
        insideColor = array.getColor(R.styleable.CustomRetangeView_defaultInsideColor, defaultInsideColor);
        numberTxtColor = array.getColor(R.styleable.CustomRetangeView_numberTxtColor, Color.BLACK);
        numberTextSize = (int) array.getDimension(R.styleable.CustomRetangeView_numberTextSize, 10);
        defaultNum = array.getInteger(R.styleable.CustomRetangeView_defaultNum, 100);
        array.recycle();

        //初始化默认进度
//        defaultNum = 50;
        recProgress = 50;
        this.paint = new Paint();
        this.paint.setAntiAlias(true);
        rec = new Rect();


    }

    private LinearGradient liquid;


    @SuppressLint("CanvasSize")
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        width = canvas.getWidth();
        height = canvas.getHeight();
        int startY = (int) dpToPx(15);//底部留出15dp，进度为0，也依然有15进度
        int paddingHeight = height - startY * 2;//底部和顶部留出的距离

        recHeight = paddingHeight;
        rowWidth = width;

        //绘制外面默认柱子
//        paint.setColor(recColor);
//        canvas.drawRoundRect(0, height - recHeight - startY, rowWidth, height, 10, 10, paint);

        // 绘制里面的柱子高度
        insideHeight = (paddingHeight * recProgress / 100);
//        paint.setColor(insideColor);
//        canvas.drawRoundRect(0, height - insideHeight - startY, rowWidth, height, 10, 10, paint);


        liquid = new LinearGradient(0.0f, 0.0f, (float) rowWidth, (float) height, liquidColors, null, Shader.TileMode.REPEAT);
        paint.setShader(liquid);
        canvas.drawRoundRect(0, height - recHeight, rowWidth, height, 10, 10, paint);

        //绘制顶部文字
        paint.setColor(numberTxtColor);
        paint.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        paint.setTextSize(spToPx(numberTextSize));
        String weightValue = defaultNum + "g";
        paint.getTextBounds(weightValue, 0, weightValue.length(), rec);
        // 文字宽度
        int textWidth = rec.width();
        canvas.drawText(weightValue, rowWidth / 2 - textWidth / 2, height - recHeight - startY - 5, paint);


    }

    private float spToPx(int sp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, getContext().getResources().getDisplayMetrics());
    }

    private float dpToPx(int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getContext().getResources().getDisplayMetrics());
    }


    /**
     * 设置数据
     *
     * @param percent 所占百分比
     */
    public void setValue(int percent) {
        this.recProgress = percent;
        invalidate();
    }

    /**
     * 设置数据和颜色
     *
     * @param percent 所占百分比
     * @param color   该柱子的里面颜色
     */
    public void setValueColor(int percent, int color) {
        this.recProgress = percent;
        this.insideColor = color;
        invalidate();
    }

}
